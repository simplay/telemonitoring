# AM2302 Sensor

+ Red wire connects to pin 1
+ Black wire connects to pin 6
+ Yellow wire connects to GPIO4 pin 7 (you can connect this wire to any of the GPIO pins, just make note of which one as we need to know which pin to listen on)

<img src="docs/gpio_layout_rpi.jpg" width="400" />

## Setup

`sudo pip3 install Adafruit_DHT`

## Usage

`python3 AdafruitDHT.py 2302 4`

## Sources

+ https://github.com/adafruit/Adafruit_Python_DHT
+ https://www.pi-shop.ch/temperatur-und-feuchtigkeitssensor
