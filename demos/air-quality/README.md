# TVOC/eCO2

<img src="docs/gpio_layout_rpi.jpg" width="400" />

<img src="docs/sensor.jpg" width="400" />

## Setup

`pip3 install -r requirements.txt`

## Usage

`python3 demo.py`

## Sources

+ https://www.pi-shop.ch/air-quality-sensor-breakout
+ https://github.com/pimoroni/sgp30-python
