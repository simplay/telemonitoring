# BH1745 Sensor

<img src="docs/gpio_layout_rpi.jpg" width="400" />

<img src="docs/sensor.jpg" width="400" />

## Setup

`pip3 install -r requirements.txt`

## Usage

`python3 demo.py`

# Sources

+ https://www.pi-shop.ch/luminance-and-colour-sensor-breakout-bh1745
+ https://github.com/pimoroni/bh1745-python
